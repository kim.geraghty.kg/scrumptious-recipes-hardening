from django.db import models
# from django.core.validators import MaxValueValidator, MinValueValidator

# from django.contrib.auth.models import User
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField() # how to enter a future date?, not needed?
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plans",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="meal_plans")


    def __str__(self):
        return self.name
