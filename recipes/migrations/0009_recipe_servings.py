# Generated by Django 4.0.3 on 2022-09-02 17:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_shoppingitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='servings',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
