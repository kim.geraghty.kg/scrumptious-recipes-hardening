from django import template

register = template.Library()


def resize_to(ingredient, target):
    ratio = 0
    num_servings = ingredient.recipe.servings
    print(num_servings)
    if num_servings is not None and target is not None:
        try:
            ratio = int(target) / int(num_servings)
            return ratio * ingredient.amount
        except AttributeError:
            pass
    return int(ingredient.amount)


register.filter(resize_to)
